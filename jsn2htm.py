from json2html import *
import json
# from collections import OrderedDict

# def get_elements(json_txt):
#     arr = json.loads(json_txt)
#     new = {}
#     list_of_keys = []
#     list_of_keys_from_dicts = [list(elem.keys()) for elem in arr]
#     # getting keys from json
#     for keys in list_of_keys_from_dicts:
#         for key in keys:
#             if key not in list_of_keys:
#                 list_of_keys.append(key)
#     for key in list_of_keys:
#         new[key] = []
#     for element in arr:
#         for key in list_of_keys:
#             if key in element:
#                 new[key].append(element[key])
#             else:
#                 new[key].append(None)
#     return json.dumps(new)


__version__ = '$1.0.2018.08.20.1102 indev python3.7$'
__date__ = '14.08.2018'
__author__ = 'Unkraut77 (https://gitlab.com/no_more_secrets/)'


def deleteJSONKey(jsonData, keylist):
    if len(keylist) > 1:
        if jsonData.get(keylist[0]) is not None:
            jsonData[keylist[0]] = deleteJSONKey(jsonData[keylist[0]], keylist[1:])
    else:
        try:
            if keylist[0] in jsonData:
                del jsonData[keylist[0]]
            else:
                for item in jsonData:
                    if keylist[0] in item and type(item) == dict:
                        del item[keylist[0]]
        except ValueError as ve:
            print(ve)
            pass
        except TypeError as te:
            print(te)
            pass
    return jsonData


def parse(jsonContent):
    try:
        return json.load(jsonContent)  # , object_pairs_hook=OrderedDict generiert eine Liste, die nicht über get() anzusprechen ist....
    except ValueError as ve:
        print('fehlerhaftes json: {}'.format(ve))
        return None     # or: raise


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(prog='jsn2htm',
                                     description='gibt die Struktur einer JSON Datei im HTML-Tabellenformat aus\nund entfernt dabei die angegebenen Schlüssel in der erzeugten HTML-Datei.',
                                     epilog='Benötigt die Bibliothek json2html: pip install json2html')
    parser.add_argument('filesource',
                        type=argparse.FileType('r', encoding='UTF-8'),
                        help='Pfad der JSON-Datei (Datei wird nicht verändert)',
                        metavar='DATEINAME.json'
                        )
    parser.add_argument('--keytodelete', '-key',
                        help='ein oder mehrere Schlüssel in Punkt-Schreibweise (default: %(default)s)',
                        default=[],
                        nargs='*',
                        metavar='x y.z a.b.c'
                        )
    parser.add_argument('--fileoutput', '-out',
                        type=argparse.FileType('w+', encoding='UTF-8'),
                        help='Pfad der HTML-Ausgabedatei. Datei wird überschrieben, oder neu erstellt. Falls nicht angegeben wird eine gleichnamige HTML-Datei im gleichen Verzeichnis erstellt.',
                        metavar='????.htm',
                        )
    parser.add_argument('--tablehtml', '-t',
                        help='wenn angegeben, wird nur die HTML-Tabelle erzeugt, ansonsten ein vollständiges HTML-Dokument',
                        action='store_true',
                        )

    arguments = parser.parse_args()

    jsonObject = parse(arguments.filesource)
    for key in arguments.keytodelete:
        deleteJSONKey(jsonObject, key.split('.'))
    strChunkHTMLcss = '<head><style>td, th { vertical-align:top;} th {background-color: #eee;} table {border-collapse: collapse;} h1 {text-align:left;}</style></head>'
    strChunkHTMLBeginn = '<!DOCTYPE html><html>' + strChunkHTMLcss + '<body><h1>'+arguments.filesource.name+'</h1><br>'
    strChunkHTMLEnde = '</body></html>'

    strHtmlOutput = json2html.convert(json.dumps(jsonObject, sort_keys=True), clubbing=True, escape=True)

    try:
        if arguments.fileoutput:
            strOutput = arguments.fileoutput.name
            f = arguments.fileoutput
        else:
            strHtmlFileOutput, _ = arguments.filesource.name.split('.')  # [:0]
            strHtmlFileOutput += '.htm'
            strOutput = strHtmlFileOutput
            f = open(strHtmlFileOutput, encoding="utf_8", mode="w+", errors="replace")
    except OSError as oe:
        print("Fehler beim Öffnen der Ausgabedatei {}: {}".format(strOutput, oe))
        pass

    try:
        if arguments.tablehtml:
            f.write(strHtmlOutput)
        else:
            f.write(strChunkHTMLBeginn + strHtmlOutput + strChunkHTMLEnde)
        print('Ausgabe abgeschlossen: ' + f.name)
    except OSError as oe:
        print("Fehler beim Schreiben der Ausgabedatei {}: {}".format(strHtmlFileOutput, oe))
        pass

    f.close()

    sys.exit()
