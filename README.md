# jsn2htm

Python-Skript gibt die Struktur einer JSON Datei im HTML-Tabellenformat aus und entfernt dabei die angegebenen Schlüssel in der erzeugten HTML-Datei.

# Bsp.:
* jsn2htm -h
* jsn2htm emails.json   
    (erstellt gleichnamige vollständige HTML-Datei im gleichen Verzeichnis)
* jsn2htm emails.json -keytodelete status messageInfo.decos response.contentBlock.content.items.extAcctUid   
    (erstellt gleichnamige vollständige HTML-Datei im gleichen Verzeichnis und entfernt die Keys aus der HTML-Tabelle)
* jsn2htm d:\emails.json -key status messageInfo.decos -out d:\ausgabe\bereinigte-emails.htm  
    (erstellt neubenannte vollständige HTML-Datei, entfernt den Key 'status' aus der Tabelle)
* jsn2htm d:\router-config.json -t    
    (erstellt d:\router-config.htm als reine Tabelle)

# Parameter:

    jsn2htm [-h]
            [--keytodelete|-key [x y.z a.b.c [x y.z a.b.c ...]]]
            [--fileoutput|-out ????.htm]
            [--tablehtml|-t]
            DATEINAME.json


gibt die Struktur einer JSON Datei im HTML-Tabellenformat aus und entfernt
dabei die angegebenen Schlüssel in der erzeugten HTML-Datei.


# positional arguments:

    DATEINAME.json        Pfad der JSON-Datei (Datei wird nicht verändert)

# optional arguments:

    -h, --help          show this help message and exit
    --keytodelete [x y.z a.b.c [x y.z a.b.c ...]], -key [x y.z a.b.c [x y.z a.b.c ...]]
                        ein oder mehrere Schlüssel in Punkt-Schreibweise    (default: [])
    --fileoutput ????.htm, -out ????.htm
                        Pfad der HTML-Ausgabedatei. Datei wird überschrieben,
                            oder neu erstellt. Falls nicht angegeben wird eine
                            gleichnamige HTML-Datei im gleichen Verzeichnis erstellt.
    --tablehtml, -t     wenn angegeben, wird nur die HTML-Tabelle erzeugt,
                            ansonsten ein vollständiges HTML-Dokument


# Benötigt die Bibliothek json2html:

    c:\> pip install json2html